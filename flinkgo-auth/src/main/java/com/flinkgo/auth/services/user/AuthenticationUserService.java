package com.flinkgo.auth.services.user;

import com.flinkgo.auth.dto.user.AuthUser;

public interface AuthenticationUserService {
    AuthUser getUserFromContext();
}
