package com.flinkgo.auth.services.user;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.dto.user.UserRequest;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.model.user.UserInfo;
import com.flinkgo.auth.model.user.UserRole;
import com.flinkgo.auth.model.user.helper.UserStatus;
import com.flinkgo.auth.repos.UserRoleRepository;
import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.repos.userInfo.UserInfoRepository;
import com.flinkgo.auth.utilities.RandomUtils;
import com.flinkgo.auth.utilities.RestCustomException;

@Service
public class UserServiceImpl implements UserService, AuthenticationUserService {
    Logger log = Logger.getLogger(UserService.class.getName());
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    UserInfoRepository infoRepo;
    
    @Autowired
    UserRoleRepository roleRepo;
    
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Transactional
    @Override
    public User getUserAndRoleByName(String userName) {
        User user = userRepo.findByUserName(userName);
        log.info(user.getUserRole().getRoleName());
        return user;
    }

    @Transactional
    @Override
    public User getUserAndRoleByRefreshToken(String token) {
        User user = userRepo.findByRefreshToken(token);
        log.info(user.getUserRole().getRoleName());
        return user;
    }

    @Transactional
    @Override
    public User register(UserRequest req) throws RestCustomException {
        User user = new User();
        UserInfo info = new UserInfo();
        UserRole role = roleRepo.findById(2).get();
        log.info(role.getRoleName());
        
        user.setStatus(UserStatus.ACTIVE.getValue());
        user.setUserName(req.getUserName());
        user.setPassword(encoder().encode(req.getPassword()));
        user.setCreateTime(new Date());
        user.setUserRole(role);
        user.setRefreshToken(RandomUtils.genRandomString(60));
        
        
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            info.setDob(format.parse(req.getDob()));
        } catch (ParseException e) {
            throw RestCustomException.WRONG_DOB_FORMAT;
        }
        info.setGender(req.getGender());
        info.setPhilosophy(req.getPhilosophy());
        
        log.info("prepare to save");
        user = userRepo.save(user);
        
        info.setUser(user);
        info.setUserId(user.getId());
        info = infoRepo.save(info);
        user.setUserInfo(info);
        return user;
    }

    @Override
    public User save(User user) {
        return userRepo.save(user);
    }

    @Override
    public AuthUser getUserFromContext() {
        return (AuthUser) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
    }


}
