package com.flinkgo.auth.services.activateLink;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.flinkgo.auth.model.user.ActivateLink;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.repos.ActivateLinkRepository;
import com.flinkgo.auth.utilities.RandomUtils;

@Service
public class ActivateLinkServiceImpl implements ActivateLinkService {
    
    public static final long EXPIRE_TIME = 259200;
    
    private static PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Autowired
    ActivateLinkRepository linkRepo;
    
    private static String generateRandomActivateLink(User user) {
        String time = String.valueOf(System.currentTimeMillis());
        
        return encoder().encode(time + user.getUserName()) + RandomUtils.genRandomString(10);
    }

    @Transactional
    @Override
    public ActivateLink createNewAvtivateLink(User user) {
        linkRepo.removeByUserId(user.getId());
        ActivateLink link = new ActivateLink();
        link.setActivateLink(generateRandomActivateLink(user));
        link.setExpireTime(System.currentTimeMillis() + EXPIRE_TIME * 1000);        
        link.setUser(user);
        
        linkRepo.save(link);
        
        return link;
    }

    @Transactional
    @Override
    public User linkValidate(String link) {
        ActivateLink result = linkRepo.findByActivateLink(link);
        if (result == null)
            return null;
        User user = result.getUser();
        if (user != null)
            linkRepo.removeByUserId(result.getUser().getId());
        return user;
    }
}
