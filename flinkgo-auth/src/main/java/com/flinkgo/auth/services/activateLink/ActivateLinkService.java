package com.flinkgo.auth.services.activateLink;

import com.flinkgo.auth.model.user.ActivateLink;
import com.flinkgo.auth.model.user.User;

public interface ActivateLinkService {
    public ActivateLink createNewAvtivateLink(User user);
    
    public User linkValidate(String link);
}
