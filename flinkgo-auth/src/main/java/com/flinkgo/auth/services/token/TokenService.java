package com.flinkgo.auth.services.token;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.model.user.UserRole;

public interface TokenService {
    public String genJWT(User user, UserRole role, long currentTime);
    
    public DecodedJWT validateJWT(String token);

    AuthUser getUserFromJWT(DecodedJWT token);
}
