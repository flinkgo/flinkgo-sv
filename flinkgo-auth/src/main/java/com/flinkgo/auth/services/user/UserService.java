package com.flinkgo.auth.services.user;

import com.flinkgo.auth.dto.user.UserRequest;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.utilities.RestCustomException;

public interface UserService {
    
    User getUserAndRoleByName(String userName);
    User getUserAndRoleByRefreshToken(String token);
    
    User register(UserRequest userReq) throws RestCustomException;
    User save(User user);
}
