package com.flinkgo.auth.dto.user;

import java.lang.reflect.Field;

import com.flinkgo.auth.model.user.User;

public class AuthUser {

    private Long userId;
    
    private String userName;
    
    public AuthUser() {
        
    }

    public AuthUser(String userName, Long uid) {
        this.userId = uid;
        this.userName = userName;
    }
    
    public AuthUser(User user) {
        this.userId = user.getId();
        this.userName = user.getUserName();
    }

    public AuthUser(Long userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{");
        Field[] fields = this.getClass().getDeclaredFields();
        boolean firstIter = true;
        
        for (Field field : fields) {
            try {
                if (field.get(this) != null) {
                    if (!firstIter) {
                        builder.append(",");
                    }
                    firstIter = false;
                        
                    builder.append("\"" + field.getName() + "\":");
                    
                    if (field.get(this) instanceof String) {
                        builder.append("\"" + field.get(this) + "\"");
                    } else {
                        builder.append(field.get(this).toString());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
        return builder.append("}").toString();
    }
}
