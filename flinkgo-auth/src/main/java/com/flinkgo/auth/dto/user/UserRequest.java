package com.flinkgo.auth.dto.user;

import javax.validation.constraints.NotEmpty;

public class UserRequest {

    @NotEmpty
    String userName;
    
    @NotEmpty
    String password;
    
    @NotEmpty
    String gender;
    
    @NotEmpty
    String dob;
    
    String avatar;
    
    String philosophy;
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhilosophy() {
        return philosophy;
    }

    public void setPhilosophy(String philosophy) {
        this.philosophy = philosophy;
    }
}
