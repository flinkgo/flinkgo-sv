package com.flinkgo.auth.model.user.helper;

public enum UserStatus {
    INACTIVE(0),
    ACTIVE(1);
    
    final int value;
    
    UserStatus(int value) {
        this.value = value;
    }
    
    public int getValue() {
        return value;
    }
}
