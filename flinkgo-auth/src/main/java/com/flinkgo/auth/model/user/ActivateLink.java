package com.flinkgo.auth.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="activate_link")
public class ActivateLink implements Serializable {
    @Id
    @Column(name = "activate_link", unique = true, nullable = false)
    private String activateLink;
    
    @Column(name = "expire_time", nullable = false)
    private Long expireTime;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public String getActivateLink() {
        return activateLink;
    }

    public void setActivateLink(String activateLink) {
        this.activateLink = activateLink;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    
}
