package com.flinkgo.auth.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.flinkgo.auth.model.user.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long>, CrudRepository<UserRole, Long> {
    Optional<UserRole> findById(Integer roleId);
}
