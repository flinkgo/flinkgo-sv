package com.flinkgo.auth.repos.userInfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.flinkgo.auth.model.user.UserInfo;

public interface UserInfoRepository extends CrudRepository<UserInfo, Long>,
         JpaRepository<UserInfo, Long> {

}
