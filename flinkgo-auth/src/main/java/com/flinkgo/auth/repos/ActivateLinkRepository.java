package com.flinkgo.auth.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

import com.flinkgo.auth.model.user.ActivateLink;

public interface ActivateLinkRepository extends JpaRepository<ActivateLink, String>, JpaSpecificationExecutor<ActivateLink>{
    ActivateLink findByActivateLink(String activateLink);
    
    @Transactional
    List<ActivateLink> removeByUserId(Long userId);
}
