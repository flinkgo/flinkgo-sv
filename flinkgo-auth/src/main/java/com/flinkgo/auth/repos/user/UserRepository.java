package com.flinkgo.auth.repos.user;


import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.flinkgo.auth.model.user.User;

public interface UserRepository extends JpaRepository<User, Long>, CrudRepository<User, Long> {
    User findByUserName(String userName);
    
    Page<User> findAll(Pageable page);
    
    User findByRefreshToken(String token);
    
    List<User> findByIdIn(Collection<Long> ids);

}
