package com.flinkgo.dto;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.flinkgo.auth.dto.user.AuthUser;

public class AuthUserSerializeTest {

    @Test
    public void testToString() {
        AuthUser auser = new AuthUser("name", 1L);
        assertEquals(auser.toString(), "{\"userId\":1,\"userName\":\"name\"}");
    
    }
    
    @Test
    public void testNullName() {
        AuthUser auser = new AuthUser(null, 1L);
        assertEquals(auser.toString(), "{\"userId\":1}");
    }
    
    @Test
    public void testEmptyObject() {
        AuthUser auser = new AuthUser();
        assertEquals(auser.toString(), "{}");
    }

}
