package com.flinkgo.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.model.user.UserRole;
import com.flinkgo.auth.services.token.JWTTokenService;

public class JWTTokenServiceTest {
    
    private static final String FLINKGO = "flinkgo";
    private static final String UID = "uid";
    private static final String USERNAME = "user";
    
    private static final Logger log = LoggerFactory.getLogger(JWTTokenServiceTest.class);
    
    JWTTokenService sv = new JWTTokenService();
    User user = new User();
    UserRole role = new UserRole();
    Long expiredTime = System.currentTimeMillis() - 2000000;
    String token;
    String expiredToken;

    @Before
    public void setUp() throws Exception {
        log.info(System.getenv("RSA_KEY_PATH"));
        ReflectionTestUtils.setField(sv, "keyFolder", System.getenv("RSA_KEY_PATH"));
        ReflectionTestUtils.invokeMethod(sv, "initKeys");
        
        
        user.setId(1L);
        user.setUserName("user");
        
        role.setId(1);
        role.setRoleName("user");
        
        token = sv.genJWT(user, role, System.currentTimeMillis());
        expiredToken = sv.genJWT(user, role, expiredTime);
    }


    @Test
    public void testJWT() {
        DecodedJWT jwt = sv.validateJWT(token);
        
        assertEquals(Integer.valueOf(1), jwt.getClaim(UID).asInt());
        assertEquals("user", jwt.getClaim(USERNAME).asString());
        assertEquals(FLINKGO, jwt.getIssuer());
    }
    
    @Test(expected = JWTVerificationException.class)
    public void testExpiredJWT() {
        sv.validateJWT(expiredToken);
    }

    @Test
    public void testGetUserFromJWT() {
        AuthUser authUser = sv.getUserFromJWT(sv.validateJWT(token));
        
        assertEquals(Long.valueOf(1), authUser.getUserId());
        assertEquals("user", authUser.getUserName());
    }

}
