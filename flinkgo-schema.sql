-- MySQL Script generated by MySQL Workbench
-- Thứ hai, 04 Tháng ba Năm 2019 20:45:50 +07
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema flinkgo_testdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `role_description` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `role_name_UNIQUE` (`role_name` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `created_time` TIMESTAMP NULL DEFAULT NULL,
  `user_name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `password` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `user_role_id` INT(11) NULL DEFAULT NULL,
  `refresh_token` CHAR(80) CHARACTER SET 'utf8' NULL DEFAULT NULL,
  `avatar` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL,
  `status` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_lqjrcobrh9jc8wpcar64q1bfh` (`user_name` ASC),
  UNIQUE INDEX `refresh_token_UNIQUE` (`refresh_token` ASC),
  INDEX `role_idx` (`user_role_id` ASC),
  CONSTRAINT `role`
    FOREIGN KEY (`user_role_id`)
    REFERENCES `user_role` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 504
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `activate_link`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `activate_link` (
  `activate_link` VARCHAR(80) CHARACTER SET 'utf8' NOT NULL,
  `expire_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`activate_link`),
  UNIQUE INDEX `activate_linkcol_UNIQUE` (`activate_link` ASC),
  INDEX `fk_activate_link_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_activate_link_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `chat_room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `chat_room` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `active` BIT(1) NULL DEFAULT NULL,
  `created_date` TIMESTAMP NULL DEFAULT NULL,
  `created_user_id` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL,
  `last_message_time` TIMESTAMP NULL DEFAULT NULL,
  `last_message_id` INT(11) NULL DEFAULT NULL,
  `direct_chat_id` CHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `direct_room_id_UNIQUE` (`direct_chat_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 96
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `chat_room_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `chat_room_user` (
  `chat_room_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  INDEX `FK368skiewasavvt4ltyep63dn8` (`user_id` ASC),
  INDEX `FKn7wfsq1ii61la6vi9gigw4pk1` (`chat_room_id` ASC),
  CONSTRAINT `FK368skiewasavvt4ltyep63dn8`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FKn7wfsq1ii61la6vi9gigw4pk1`
    FOREIGN KEY (`chat_room_id`)
    REFERENCES `chat_room` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `comment` (
  `id_comment` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NULL DEFAULT NULL,
  `image_link` VARCHAR(255) NULL DEFAULT NULL,
  `time_comment` DATETIME NULL DEFAULT NULL,
  `id_post` INT(11) NULL DEFAULT NULL,
  `id_user_comment` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_comment`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `comment_reply`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `comment_reply` (
  `id_comment_reply` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NULL DEFAULT NULL,
  `image_link` VARCHAR(255) NULL DEFAULT NULL,
  `time_comment` DATETIME NULL DEFAULT NULL,
  `id_comment` INT(11) NULL DEFAULT NULL,
  `id_user_comment` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_comment_reply`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `feeling`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `feeling` (
  `id_feeling` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_feeling`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `message` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `chat_room_id` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `user_name` VARCHAR(255) NULL DEFAULT NULL,
  `created_time` TIMESTAMP NOT NULL,
  `message` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `post` (
  `id_post` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NOT NULL,
  `image_link` VARCHAR(255) NOT NULL,
  `score` INT(11) NOT NULL,
  `time_post` DATETIME NULL DEFAULT NULL,
  `id_feeling` INT(11) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_post`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `user_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_info` (
  `user_id` INT(11) NOT NULL,
  `gender` CHAR(20) NULL DEFAULT NULL,
  `dob` DATE NULL DEFAULT NULL,
  `philosophy` VARCHAR(500) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_info_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
