package com.flinkgo.sv.message;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageProducer {
    
    @Autowired
    RabbitTemplate template;
    
    @Autowired
    AmqpAdmin amqpAdmin;
    
    public void sendMessage(String msg, String queueName) {
        TopicExchange exchange = new TopicExchange("flinkgo_topic", false, false);
        Queue queue = new Queue(queueName, false, false, false);
        Binding binding = BindingBuilder.bind(queue).to(exchange).with(queueName);
        amqpAdmin.declareExchange(exchange);
        amqpAdmin.declareQueue(queue);
        amqpAdmin.declareBinding(binding);
        
        template.convertAndSend(queueName, msg);
    }
}