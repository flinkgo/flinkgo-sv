package com.flinkgo.sv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flinkgo.sv.message.MessageProducer;

@RestController
@RequestMapping("/")
public class TestController {
    
    @Autowired 
    MessageProducer msgProducer;
    
    @GetMapping
    public String success() {
        return "flinkgo api ok";
    }
    
    @GetMapping("send")
    public String sendMessage(@RequestParam String msg) {
        
        msgProducer.sendMessage(msg, "flinkgo");
        return msg;
    }
}
