package com.flinkgo.sv.controller;

import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.flinkgo.auth.dto.ResponseFail;
import com.flinkgo.auth.dto.ResponseSuccess;
import com.flinkgo.auth.dto.user.UserRequest;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.model.user.UserRole;
import com.flinkgo.auth.repos.UserRoleRepository;
import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.services.activateLink.ActivateLinkService;
import com.flinkgo.auth.services.user.UserService;
import com.flinkgo.auth.utilities.RestCustomException;

@RestController
@RequestMapping("/user")
public class UserController {
    static final Logger log = Logger.getLogger(UserController.class.getName());
    
    static final Integer DEFAULT_ROLE_ID = 2;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    UserService userService;
    
    @Autowired
    UserRoleRepository roleRepo;
    
    @Autowired
    ActivateLinkService linkService;
    
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
    
    @CrossOrigin
    @PostMapping("/register")
    @ResponseBody
    public final ResponseEntity<Object> create(
            @RequestBody @Valid UserRequest req) throws RestCustomException{
        
        User user = userService.register(req);
        
        ResponseSuccess res = new ResponseSuccess(HttpStatus.OK.value(), "success");
        res.addResults("user", user);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }
    
    @CrossOrigin
    @GetMapping("/get")
    public final ResponseEntity<Object> get (
            @RequestParam("userName") String userName) {

        User user = userRepo.findByUserName(userName);
        
        ResponseSuccess res = new ResponseSuccess(HttpStatus.FOUND.value(), "success");
        res.addResults("user", user);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }
    
    @CrossOrigin
    @PutMapping("/update")
    @ResponseBody
    public final ResponseEntity<Object> update (
            @RequestBody UserRequest req) {
        
        User user;
        if (req.getUserName() == null) {
            ResponseFail res = new ResponseFail(HttpStatus.BAD_REQUEST.value(), "1000", "userName empty");
            return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
        }
        user = userRepo.findByUserName(req.getUserName());
        
        if (req.getPassword() != null) {
            user.setPassword(encoder().encode(req.getPassword()));
        }
        
        ResponseSuccess res = new ResponseSuccess(HttpStatus.OK.value(), "success");
        res.addResults("user", user);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }
    
    @CrossOrigin
    @GetMapping("/list")
    @ResponseBody
    public final ResponseEntity<Object> list (
            @RequestParam(value="page", required=false) Integer page, 
            @RequestParam(value="size", required=false) Integer size) {
        log.info("requested on /user/list");
        if (page == null)
            page = 0;
        if (size == null)
            size = 10;

        Pageable pageable = PageRequest.of(page, size);
        
        Page<User> users;
        users = userRepo.findAll(pageable);
        
        for (User user : users.getContent()) {
            log.info(user.getUserName());
        }
        
        ResponseSuccess res = new ResponseSuccess(HttpStatus.OK.value(), "success");
        res.addResults("users", users.getContent());
        log.info(res.getResponseData().get("users").toString());
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }
    
    @CrossOrigin
    @GetMapping("/validate/{activateLink}")
    @ResponseBody
    public final ResponseEntity<Object> activate (
            @PathVariable("activateLink") String activateLink) {
        log.info("requested on /user/" + activateLink);
        
        User user = linkService.linkValidate(activateLink);
        
        if (user == null) {
            ResponseFail res = new ResponseFail(HttpStatus.NOT_FOUND.value(), "Not Found", "Not Found");
            return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
        } else {
            UserRole role = roleRepo.findById(DEFAULT_ROLE_ID).get();
            user.setUserRole(role);
            
            userRepo.save(user);
            
            ResponseSuccess res = new ResponseSuccess(HttpStatus.OK.value(), "success");
            res.addResults("user", user);
            log.info("activate: " + user.getUserName());
            return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
        }
    }
    
}
