package com.flinkgo.sv.config;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DebugSpringConfig {
    Logger log = Logger.getLogger(DebugSpringConfig.class.getName());

    public void postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        log.log(Level.INFO, "postProcessBeforeInitialization: bean(" + beanName + ")");
    }


    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        // log.info("postProcessAfterInitialization: bean("+beanName+")");
        return bean;
    }
}
