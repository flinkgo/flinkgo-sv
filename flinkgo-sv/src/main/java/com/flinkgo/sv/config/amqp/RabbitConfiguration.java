package com.flinkgo.sv.config.amqp;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {
    static final Logger log = LoggerFactory.getLogger(RabbitConfiguration.class);
    
    @Value("${spring.rabbitmq.host}")
    private String hostname;
    
    @Value("${spring.rabbitmq.username}")
    private String username;
    
    @Value("${spring.rabbitmq.password}")
    private String password;
    
    @Value("${spring.rabbitmq.virtual-host}")
    private String vhost;
    
    public static String TOPIC = "flinkgo_topic";
    public static String TEST_QUEUE = "flinkgo_test";
    
    @PostConstruct
    private void initVHost() {
        try {
            HttpPut req = new HttpPut("http://" + hostname + ":15672/api/vhosts/" + vhost);
            String basicAuth = "Basic " + 
                    new String(Base64.encodeBase64(String.format("%s:%s", username, password).getBytes()));
            
            log.warn("init: " + username);
            req.addHeader("Authorization", basicAuth);
            
            req.addHeader("Content-type", "application/json");
            
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse res = httpClient.execute(req);
            
            log.info("vhost create return: " + String.valueOf(res.getStatusLine().getStatusCode()));
            
            httpClient.close();
            
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
    
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory conn = new CachingConnectionFactory(hostname);
        
        log.warn(username);
        conn.setUsername(username);
        conn.setPassword(password);
        conn.setVirtualHost(vhost);
        
        return conn;
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public AmqpTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMessageConverter(jackson2MessageConverter());
        
        return template;
    }
    
    @Bean
    public Jackson2JsonMessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
    @Bean
    SimpleMessageListenerContainer container() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory());
        container.setMessageConverter(jackson2MessageConverter());
        
        return container;
    }
    
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(TOPIC);
    }
    
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(TOPIC);
    }
    
    @Bean
    Queue queue() {
        return new Queue(TEST_QUEUE);
    }
}
