package com.flinkgo.sv.config.rest;

import java.util.logging.Logger;

import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.flinkgo.auth.dto.ResponseFail;
import com.flinkgo.auth.utilities.RestCustomException;

@RestControllerAdvice(basePackages = "com.flinkgo.controller")
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    Logger log = Logger.getLogger(RestExceptionHandler.class.getName());
    
    @ExceptionHandler(value = {RestCustomException.class})
    protected ResponseEntity<Object> handlingRestException(RestCustomException e) {
        log.info("handling");
        ResponseFail res = new ResponseFail(e);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseFail res = new ResponseFail(RestCustomException.PARAMATER_INVALID);
        
        res.addMessage(ex.getBindingResult().getFieldError().getField() + " "
                + ex.getBindingResult().getFieldError().getDefaultMessage());

        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(res.getStatus()));
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseFail res = new ResponseFail(RestCustomException.PARAMATER_MISSING);

        res.addMessage(ex.getMessage());

        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(res.getStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        ResponseFail res = new ResponseFail(RestCustomException.TYPE_MISMATCHED);
        
        res.addMessage(ex.getMessage());

        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(res.getStatus()));
    }
    
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseFail res = new ResponseFail(RestCustomException.MALFORM_REQUEST);
        
        res.addMessage(ex.getMessage());
        
        
        return new ResponseEntity<Object>(res, new HttpHeaders(), HttpStatus.valueOf(res.getStatus()));
    }
}