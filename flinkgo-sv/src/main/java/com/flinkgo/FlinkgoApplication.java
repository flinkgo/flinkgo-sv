package com.flinkgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.flinkgo.sv.config.amqp.RabbitConfiguration;
import com.flinkgo.sv.config.rest.MVCConfig;


@SpringBootApplication
@Import({ MVCConfig.class, RabbitConfiguration.class })
@ComponentScan(basePackages = { "com.flinkgo" })
@EnableJpaRepositories( basePackages = { "com.flinkgo.auth.repos" } )
public class FlinkgoApplication extends SpringBootServletInitializer {

    @Override
    public SpringApplicationBuilder configure(SpringApplicationBuilder app) {
        return app.sources(FlinkgoApplication.class);
    }
    
    public static void main(String[] args) {
        SpringApplication.run(FlinkgoApplication.class, args);
    }
    
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
