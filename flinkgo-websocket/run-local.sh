#!/bin/bash
source .env
export $(cut -d= -f1 .env)

export STATE='.SNAPSHOT'
export VERSION=`git describe --abbrev=0 --tags`

DIR=$PWD/..
PRJ=$PWD
AUTH_PRJ=$DIR/flinkgo-auth

mvn clean
if [ $1 = 'run' ]
then
  cd $AUTH_PRJ && mvn clean install && \
  cd $PRJ && mvn spring-boot:run
elif [ $1 = 'build' ]
then
  if [ ! -d "$PRJ/target/key" ]
  then
    mkdir $PRJ/target
    mkdir $PRJ/target/key
  fi
  cp -r $RSA_KEY_PATH $PRJ/target/key/ && \

  if [ $2 = 'release' ]
  then
    export STATE='.RELEASE'
  fi

  cd $AUTH_PRJ && mvn clean install && \
  cd $PRJ && mvn install dockerfile:build

  rm -r $PRJ/target/key
elif [ $1 = 'test' ]
then
  cd $AUTH_PRJ && mvn clean install && \
  cd $PRJ && mvn test
else
  echo "argument invalid"
  exit 1
fi

