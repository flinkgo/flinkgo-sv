package com.flinkgo.socket;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.repos.user.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FlinkgoWebsocketApplicationTests {
    Logger log = Logger.getLogger(FlinkgoWebsocketApplicationTests.class.getSimpleName());
    
    @Autowired
    UserRepository userRepo;
    
    
    protected User user1, user2, user3;

    public User setUpUser(String userName) {
        User user = new User();
        user.setUserName(userName);
        user.setPassword("pass");
        user.setStatus(1);
        
        return userRepo.save(user);
    }
    
    @Before 
    public void setUpUsers() throws Exception {
        user1 = setUpUser("user1");
        user2 = setUpUser("user2");
        user3 = setUpUser("user3");
    }
    
    @After
    public void tearDownUsers() throws Exception {
        userRepo.delete(user1);
        userRepo.delete(user2);
        userRepo.delete(user3);
    }
    
	@Test
	public void contextLoads() {
	}
}
