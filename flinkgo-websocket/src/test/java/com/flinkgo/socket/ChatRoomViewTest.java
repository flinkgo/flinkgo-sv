package com.flinkgo.socket;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import com.flinkgo.auth.model.user.User;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomView;

public class ChatRoomViewTest {

    ChatRoom chatRoom = new ChatRoom();
    
    @Before
    public void setUpChatroom() {
        chatRoom.setId(1L);
        chatRoom.setCreatedUserId(1L);
        
        for (int i = 1; i < 4; i++) {
            User user = new User();
            user.setId(Long.valueOf(i));
            chatRoom.addUsers(user);
        }
    }
    
    @Test
    public void testViewCreate() {
        ChatRoomView view = new ChatRoomView(chatRoom);
        
        Long[] testArray = {1L, 2L, 3L};
        assertArrayEquals(view.getUserIds().toArray(), testArray);
    }
    
}
