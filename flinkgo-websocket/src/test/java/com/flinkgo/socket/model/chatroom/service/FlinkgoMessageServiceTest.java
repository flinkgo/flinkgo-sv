package com.flinkgo.socket.model.chatroom.service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.utilities.RestCustomException;
import com.flinkgo.socket.FlinkgoWebsocketApplicationTests;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.chatroom.ChatRoomMessage;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;
import com.flinkgo.socket.repository.chatroom.ChatRoomMessageRepository;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

public class FlinkgoMessageServiceTest extends FlinkgoWebsocketApplicationTests {
    private final static Logger log = LoggerFactory.getLogger(FlinkgoMessageServiceTest.class);
    
    @Autowired
    ChatRoomRepository roomRepo;
    
    @Autowired
    ChatRoomMessageRepository msgRepo;
    
    @Autowired 
    UserRepository userRepo;
    
    @Autowired
    ChatMessageService msgService;
    
    private final static String TEST_MESSAGE = "test message";
    
    ChatMessageCreation req;
    ChatRoomMessage msg;
    ChatRoom room, directRoom;
    
    @Before
    public void setUpRoom() throws Exception {
        room = new ChatRoom();
        room.setActive(true);
        room.setJoinedUsers(Arrays.asList(user1, user2, user3));
        room.setCreatedDate(new Date());
        room.setName("testRoom");
        
        room = roomRepo.save(room);
    }
    
    @Before
    public void setUpDirectInput() throws Exception {
        req = new ChatMessageCreation();
        req.setMessage(TEST_MESSAGE);
        req.setFromUserId(user1.getId());
        req.setFromUserName(user1.getUserName());
        req.setRoomId(room.getId());
    }
    
    @After
    public void tearDownRoom() throws Exception {
        roomRepo.delete(room);
        if(room != null)
            log.info(room.getId().toString());
        
        if (msg != null) {
            msgRepo.delete(msg);
        }
    }

    @Test
    public void testSaveRoomMessage() {
        req.setDirectMessage(false);
        
        msg = msgService.saveChatMessage(req);
        
        assertEquals(TEST_MESSAGE, msg.getMessage());
        
        room = roomRepo.findById(room.getId()).get();
        assertEquals(msg.getId(), room.getLastMessageId());
    }
    
    @Test(expected = RestCustomException.class)
    public void testMissingRoom() {
        req.setRoomId(0L);
        req.setDirectMessage(false);
        
        msgService.saveChatMessage(req);
    }
    
    @Test(expected = RestCustomException.class)
    public void testMissingUser() {
        req.setFromUserId(0L);
        req.setDirectMessage(false);
        
        msgService.saveChatMessage(req);
    }
    
    @Test(expected = RestCustomException.class)
    public void testMissingUserName() {
        req.setFromUserName(null);
        req.setDirectMessage(false);
        
        msgService.saveChatMessage(req);
    }
    
    @Test
    public void testSaveDirectMessage() {
        req.setDirectMessage(true);
        req.setFromUserId(user1.getId());
        req.setFromUserName(user1.getUserName());
        req.setToUserId(user2.getId());
        req.setToUserName(user2.getUserName());
        
        msg = msgService.saveChatMessage(req);
        
        assertEquals(TEST_MESSAGE, msg.getMessage());
        ChatRoom createdRoom = roomRepo.findFirstByLastMessageId(msg.getId()).get();
        log.info(room.getId().toString());
        
        assertEquals(msg.getId(), createdRoom.getLastMessageId());
        roomRepo.delete(createdRoom);
    }
    
}
