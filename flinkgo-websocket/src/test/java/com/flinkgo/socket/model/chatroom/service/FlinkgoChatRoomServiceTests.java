package com.flinkgo.socket.model.chatroom.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.flinkgo.auth.config.security.FlinkgoAuthenticationToken;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.utilities.RestCustomException;
import com.flinkgo.socket.FlinkgoWebsocketApplicationTests;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomCreateRequest;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomView;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

public class FlinkgoChatRoomServiceTests extends FlinkgoWebsocketApplicationTests {
    private final Logger log = LoggerFactory.getLogger(FlinkgoChatRoomServiceTests.class);
    
    Long roomId, roomId2;
    ChatRoomCreateRequest req;
    ChatRoomView view;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    ChatRoomService roomService;
    
    @Autowired
    ChatRoomRepository roomRepo;
    
    @Before
    public void setUpTest() throws Exception {
        req = new ChatRoomCreateRequest();
        req.setRoomName("testRoom");
        req.setUserIds(new ArrayList<Long>(Arrays.asList(user1.getId(), user2.getId())));
        
        // set up test authentication
        AuthUser authUser = new AuthUser(user1);
        Authentication auth = new FlinkgoAuthenticationToken(authUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @After
    public void tearDownTest() throws Exception {
        
        if (roomId != null) {
            if (roomId.equals(roomId2)) {
                roomId2 = null;
                log.info("the same id");
            }
            
            log.info(roomId.toString());
            roomRepo.deleteById(roomId);
            roomId = null;
        }
        
        if (roomId2 != null) {
            log.info(roomId2.toString());
            roomRepo.deleteById(roomId2);
            roomId2 = null;
        }
    }
    
    @Test(expected = RestCustomException.class)
    public void testCreateRoomUserNotExist() {
        req.setUserIds(new ArrayList<Long>(Arrays.asList(0L)));
        
        roomService.createChatRoom(req);
    }

    @Test
    public void testCreateChatRoom() {
        ChatRoomView view = roomService.createChatRoom(req);
        roomId = view.getId();
        
        assertEquals("testRoom", view.getName());
    }
    
    @Test(expected = RestCustomException.class)
    public void testRoomNotExisted() {
        roomService.addUserToChatRoom(user3.getId(), 0L);
    }
    
    @Test
    public void testAddUserToRoom() {
        ChatRoomView view = roomService.createChatRoom(req);
        roomId = view.getId();
        roomService.addUserToChatRoom(user3.getId(), roomId);
        
        ChatRoom room = roomRepo.findById(roomId).get();
        List<Long> joinedIds = room.getJoinedUsers().stream().map(User::getId).collect(Collectors.toList());
        
        assertTrue(joinedIds.contains(user3.getId()));
    }
    
    @Test
    public void testCreateDirectChatRoom() {
        ChatRoomView view1 = roomService.createDirectChatRoom(user1.getId(), user2.getId());
        roomId = view1.getId();
        
        ChatRoomView view2 = roomService.createDirectChatRoom(user2.getId(), user1.getId());
        roomId2 = view2.getId();

        assertEquals(roomId, roomId2);
    }

    @Test(expected = RestCustomException.class)
    public void testMissingUserCreateDirectChatRoom() {
        roomService.createDirectChatRoom(null, user1.getId());
        roomService.createDirectChatRoom(user1.getId(), null);
        
        roomService.createDirectChatRoom(user1.getId(), 0L);
        roomService.createDirectChatRoom(0L, user1.getId());
    }
    
}
