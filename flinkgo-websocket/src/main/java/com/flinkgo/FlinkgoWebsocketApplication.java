package com.flinkgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.flinkgo.socket.config.rest.MVCConfig;


@SpringBootApplication
@Import({ MVCConfig.class })
@ComponentScan(basePackages = { "com.flinkgo" })
@EnableJpaRepositories( basePackages = { "com.flinkgo.auth.repos", "com.flinkgo.socket.repository" } )
public class FlinkgoWebsocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlinkgoWebsocketApplication.class, args);
	}

}
