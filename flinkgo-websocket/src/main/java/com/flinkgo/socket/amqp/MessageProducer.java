package com.flinkgo.socket.amqp;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flinkgo.socket.config.amqp.RabbitConfiguration;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;

@Component
public class MessageProducer {
    
    @Autowired
    RabbitTemplate template;
    
    @Autowired
    AmqpAdmin amqpAdmin;
    
    public void sendMessage(String msg, String queueName) {
        TopicExchange exchange = new TopicExchange("flinkgo_topic", false, false);
        Queue queue = new Queue(queueName, false, false, false);
        Binding binding = BindingBuilder.bind(queue).to(exchange).with(queueName);
        amqpAdmin.declareExchange(exchange);
        amqpAdmin.declareQueue(queue);
        amqpAdmin.declareBinding(binding);
        
        template.convertAndSend(queueName, msg);
    }
    
    public void sendTestMessage(String msg) {
        template.convertAndSend(RabbitConfiguration.TEST_QUEUE, msg);
    }
    
    public void sendSaveMessage(ChatMessageCreation msg) {
        template.convertAndSend(RabbitConfiguration.SAVE_MSG_QUEUE, msg);
    }
}