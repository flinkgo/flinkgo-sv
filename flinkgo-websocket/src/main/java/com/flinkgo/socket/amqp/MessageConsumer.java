package com.flinkgo.socket.amqp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flinkgo.socket.config.amqp.RabbitConfiguration;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;
import com.flinkgo.socket.model.chatroom.service.ChatMessageService;

@Component
public class MessageConsumer {
    private final static Logger log = LoggerFactory.getLogger("AMPQListener");
    
    @Autowired
    ChatMessageService chatMsgService;
    
    @RabbitListener(queues = {RabbitConfiguration.TEST_QUEUE})
    public void receiveMessage(String message) {
        log.info(message);
    }
    
    @RabbitListener(queues = {RabbitConfiguration.SAVE_MSG_QUEUE})
    public void saveChatMessage(ChatMessageCreation msg) {
        chatMsgService.saveChatMessage(msg);
    }
}
