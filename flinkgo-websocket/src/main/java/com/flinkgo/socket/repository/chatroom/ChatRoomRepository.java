package com.flinkgo.socket.repository.chatroom;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.flinkgo.socket.model.chatroom.ChatRoom;

public interface ChatRoomRepository extends JpaRepository<ChatRoom, Long>,
        CrudRepository<ChatRoom, Long> {
    
    @Query(nativeQuery = true,
            value = "select * from chat_room cr \n" + 
                    "inner join chat_room_user cru on cr.id = cru.chat_room_id\n" + 
                    "where cru.user_id = ?1\n" + 
                    "limit ?2 offset ?3")
    List<ChatRoom> getRoomsByJoinedUser(Long userId, Integer size, Integer offset);
    
    Optional<ChatRoom> findFirstByDirectRoomId(String roomId);
    
    Optional<ChatRoom> findFirstByLastMessageId(Long messageId);
}
