package com.flinkgo.socket.repository.chatroom;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.flinkgo.socket.model.chatroom.ChatRoomMessage;

public interface ChatRoomMessageRepository extends JpaRepository<ChatRoomMessage, Integer> {
    List<ChatRoomMessage> findAllByChatRoomId(Integer id, Pageable page);
}
