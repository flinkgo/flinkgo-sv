package com.flinkgo.socket.model.chatroom.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.message.dto.ReturnedMessage;

public class ChatRoomView {

    private Long id;

    private String name;

    private List<Long> userIds;

    private List<ReturnedMessage> messages;

    public ChatRoomView() {
        super();
    }

    public ChatRoomView(ChatRoom chatRoom) {
        this.id = chatRoom.getId();
        this.name = chatRoom.getName();
        this.userIds = chatRoom.getJoinedUsers().stream().map(User::getId).collect(Collectors.toList());

        this.room = chatRoom;
    }

    @JsonIgnore
    private ChatRoom room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public List<ReturnedMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ReturnedMessage> messages) {
        this.messages = messages;
    }

    public ChatRoom getRoom() {
        return room;
    }

}
