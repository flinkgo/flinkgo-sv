package com.flinkgo.socket.model.chatroom.dto;

import com.flinkgo.socket.model.message.dto.ReturnedMessage;

public class ChatMessageCreation extends ReturnedMessage {
    private Boolean directMessage;
    
    public ChatMessageCreation() {
        super();
    }
    
    public ChatMessageCreation(ReturnedMessage msg, boolean isDirect) {
        super(msg);
        
        this.directMessage = isDirect;
    }
    
    public Boolean isDirectMessage() {
        return directMessage;
    }

    public void setDirectMessage(Boolean directMessage) {
        this.directMessage = directMessage;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public Boolean getDirectMessage() {
        return directMessage;
    }
}
