package com.flinkgo.socket.model.message.service;

import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.socket.model.message.dto.UploadMessage;

public interface ChatMessageService {
    void sendMessageToUser(UploadMessage message, AuthUser fromUser) throws Exception;
    
    void sendMessageToRoom(UploadMessage message, AuthUser fromUser, Long roomId) throws Exception;
}
