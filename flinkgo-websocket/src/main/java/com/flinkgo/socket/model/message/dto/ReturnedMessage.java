package com.flinkgo.socket.model.message.dto;

import com.flinkgo.auth.dto.user.AuthUser;

public class ReturnedMessage extends UploadMessage {
    
    protected Long fromUserId;
    
    protected String fromUserName;
    
    protected Long roomId;
    
    public ReturnedMessage() {
        super();
    }
    
    public ReturnedMessage(ReturnedMessage msg) {
        super(msg);
        
        this.toUserId = msg.getToUserId();
        this.toUserName = msg.getToUserName();
        this.fromUserId = msg.getFromUserId();
        this.fromUserName = msg.getFromUserName();
        this.roomId = msg.getRoomId();
    }
    
    public ReturnedMessage(String msg, AuthUser fromUser) {
        this.setMessage(msg);
        this.fromUserId = fromUser.getUserId();
        this.fromUserName = fromUser.getUserName();
    }
    
    public ReturnedMessage(String msg, AuthUser fromUser, AuthUser toUser) {
        this.setMessage(msg);
        this.fromUserId = fromUser.getUserId();
        this.fromUserName = fromUser.getUserName();
        this.toUserId = toUser.getUserId();
        this.toUserName = toUser.getUserName();
    }
    
    public ReturnedMessage(String msg, AuthUser fromUser, Long roomId) {
        this.setMessage(msg);
        this.fromUserId = fromUser.getUserId();
        this.fromUserName = fromUser.getUserName();
        this.roomId = roomId;
    }
    
    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long userId) {
        this.fromUserId = userId;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String userName) {
        this.fromUserName = userName;
    }
    
    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long chatRoomId) {
        this.roomId = chatRoomId;
    }
}
