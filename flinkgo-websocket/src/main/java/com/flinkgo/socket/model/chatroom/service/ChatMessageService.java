package com.flinkgo.socket.model.chatroom.service;

import com.flinkgo.socket.model.chatroom.ChatRoomMessage;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;

public interface ChatMessageService {
    
    public ChatRoomMessage saveChatMessage(ChatMessageCreation req);    
    
}
