package com.flinkgo.socket.model.message.dto;

public class UploadMessage extends ChatMessageAbstract {
    
    protected Long toUserId;
    
    protected String toUserName;
    
    public UploadMessage() {
        super();
    }
    
    public UploadMessage(UploadMessage msg) {
        super(msg);
        
        this.toUserId = msg.getToUserId();
        this.toUserName = msg.getToUserName();
    }

    public UploadMessage(String message, Long toUserId, String toUserName) {
        this.setMessage(message);
        this.toUserId = toUserId;
        this.toUserName = toUserName;
    }
    
    public Long getToUserId() {
        return toUserId;
    }

    public String getToUserName() {
        return toUserName;
    }
}
