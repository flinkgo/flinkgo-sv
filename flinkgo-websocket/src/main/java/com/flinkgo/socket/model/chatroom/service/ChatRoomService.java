package com.flinkgo.socket.model.chatroom.service;

import java.util.List;

import com.flinkgo.socket.model.chatroom.dto.ChatRoomCreateRequest;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomView;

public interface ChatRoomService {
    ChatRoomView createChatRoom(ChatRoomCreateRequest req);
    
    ChatRoomView createDirectChatRoom(Long fromUser, Long toUser);

    List<ChatRoomView> getChatRoomView(Integer page, Integer size);
    
    void addUserToChatRoom(Long userId, Long roomId);
}
