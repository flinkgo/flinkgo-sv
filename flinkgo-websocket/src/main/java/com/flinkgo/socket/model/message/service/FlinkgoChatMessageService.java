package com.flinkgo.socket.model.message.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.socket.amqp.MessageProducer;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;
import com.flinkgo.socket.model.message.dto.ReturnedMessage;
import com.flinkgo.socket.model.message.dto.UploadMessage;

@Service
public class FlinkgoChatMessageService implements ChatMessageService {
    
    static final Logger log = LoggerFactory.getLogger(FlinkgoChatMessageService.class);

    @Autowired 
    ObjectMapper mapper;
    
    @Autowired
    SimpMessagingTemplate msgTemplate;
    
    @Autowired
    MessageProducer amqpMsg;
    
    @Override
    public void sendMessageToUser(UploadMessage message, AuthUser fromUser) throws Exception {
        AuthUser user = new AuthUser(message.getToUserId(), message.getToUserName());
        ReturnedMessage returnMessage = new ReturnedMessage(message.getMessage(), fromUser, user);
        
        msgTemplate.convertAndSendToUser(mapper.writeValueAsString(fromUser), "/queue/reply", 
                returnMessage);
        
        msgTemplate.convertAndSendToUser(mapper.writeValueAsString(user), "/queue/reply",
                returnMessage);
        
        amqpMsg.sendSaveMessage(new ChatMessageCreation(returnMessage, true));
    }

    @Override
    public void sendMessageToRoom(UploadMessage message, AuthUser fromUser, Long roomId) throws Exception {
        ReturnedMessage returnMessage = new ReturnedMessage(message.getMessage(), fromUser, roomId);
        msgTemplate.convertAndSend("/topic/room." + roomId, returnMessage);
        
        amqpMsg.sendSaveMessage(new ChatMessageCreation(returnMessage, false));
    }
}
