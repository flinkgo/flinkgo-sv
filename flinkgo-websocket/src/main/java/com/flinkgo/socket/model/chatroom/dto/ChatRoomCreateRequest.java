package com.flinkgo.socket.model.chatroom.dto;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ChatRoomCreateRequest {
    
    @NotEmpty
    @Size(max = 30)
    private List<Long> userIds;
    
    private String roomName;

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
