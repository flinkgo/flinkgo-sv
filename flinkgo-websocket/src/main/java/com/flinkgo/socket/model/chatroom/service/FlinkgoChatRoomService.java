package com.flinkgo.socket.model.chatroom.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.services.user.AuthenticationUserService;
import com.flinkgo.auth.utilities.RestCustomException;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomCreateRequest;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomView;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

@Service
public class FlinkgoChatRoomService implements ChatRoomService {
    private static final Logger log = LoggerFactory.getLogger(FlinkgoChatRoomService.class);

    @Autowired
    AuthenticationUserService userService;

    @Autowired
    UserRepository userRepo;

    @Autowired
    ChatRoomRepository roomRepo;

    @Override
    public ChatRoomView createChatRoom(ChatRoomCreateRequest req) {
        ChatRoom chatRoom = new ChatRoom();

        Optional<AuthUser> authUserOpt = Optional.ofNullable(userService.getUserFromContext());

        authUserOpt.map(authUser -> {
            chatRoom.setCreatedUserId(authUser.getUserId());
            return authUser;
        }).orElseThrow(() -> RestCustomException.ACCESS_DENIED);

        List<User> users = userRepo.findByIdIn(req.getUserIds());
        if (users == null || users.isEmpty()) {
            throw RestCustomException.RESOURCE_NOT_FOUND;
        }
        chatRoom.setJoinedUsers(users);

        chatRoom.setName(req.getRoomName());

        roomRepo.save(chatRoom);

        return new ChatRoomView(chatRoom);
    }

    @Override
    public List<ChatRoomView> getChatRoomView(Integer page, Integer size) {
        Optional<AuthUser> authUserOpt = Optional.ofNullable(userService.getUserFromContext());

        List<ChatRoom> rooms = authUserOpt
                .map(authUser -> roomRepo.getRoomsByJoinedUser(authUser.getUserId(), size, page * size))
                .orElseThrow(() -> RestCustomException.ACCESS_DENIED);

        return rooms.stream().map(room -> new ChatRoomView(room)).collect(Collectors.toList());
    }

    @Override
    public void addUserToChatRoom(Long userId, Long roomId) throws RestCustomException {
        log.info("userId:" + userId);
        User user = userRepo.findById(userId)
                .orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);
        
        ChatRoom room = roomRepo.findById(roomId)
                .orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);
        
        // check if user exist in room
        Optional<AuthUser> authUserOpt = Optional.ofNullable(userService.getUserFromContext());
        boolean userInRoom = authUserOpt.map(authUser -> userIsInChatRoom(authUser, room))
                .orElseThrow(() -> RestCustomException.ACCESS_DENIED);
        if (!userInRoom)
            throw RestCustomException.ACCESS_DENIED;
            
        Set<User> userSet = new HashSet<User>();
        for (User userTemp : room.getJoinedUsers()) {
            userSet.add(userTemp);
        }
        
        userSet.add(user);
        
        List<User> users = new ArrayList<User>();
        for (User userTemp : userSet) {
            users.add(userTemp);
        }
        room.setJoinedUsers(users);
        
        roomRepo.save(room);
    }
    
    private boolean userIsInChatRoom(AuthUser user, ChatRoom room) {
        return room.getJoinedUsers().stream().map(User::getId).collect(Collectors.toList()).contains(user.getUserId());
    }

    @Override
    public ChatRoomView createDirectChatRoom(Long fromUser, Long toUser) {
        if (fromUser == null || toUser == null) {
            throw RestCustomException.PARAMATER_MISSING;
        }
        
        List<User> users = new LinkedList<>();
        
        userRepo.findById(fromUser).map(user -> users.add(user))
                .orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);
        userRepo.findById(toUser).map(user -> users.add(user))
                .orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);
        
        ChatRoom room = new ChatRoom();
        room.setDirectRoomId(generateDirectChatId(fromUser, toUser));
        room.setJoinedUsers(users);
        
        Optional<ChatRoom> roomOpt = roomRepo.findFirstByDirectRoomId(room.getDirectRoomId());
        
        ChatRoomView view;
        if (roomOpt.isPresent()) {
            view = new ChatRoomView(roomOpt.get()); 
        } else {
            view = new ChatRoomView(roomRepo.save(room));
            log.info("view: " + view.getId());
        }
        
        return view;
    }
    
    private String generateDirectChatId(Long userId1, Long userId2) {

        String hex1 = StringUtils.leftPad(Long.toHexString(userId1), 16, '0');
        String hex2 = StringUtils.leftPad(Long.toHexString(userId2), 16, '0');
        
        String id = userId1 > userId2 ? hex1 + hex2 : hex2 + hex1;

        return id;
    }
    
}
