package com.flinkgo.socket.model.chatroom;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.flinkgo.auth.model.user.User;

@Entity
@Table(name = "chat_room")
public class ChatRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "created_date")
    private Date createdDate = new Date();
    
    @Column(name = "active")
    private boolean active = true;
    
    @Column(name = "created_user_id")
    private Long createdUserId;
    
    @Column(name = "last_message_time")
    private Date lastMessageTime;
    
    @Column(name = "last_message_id")
    private Long lastMessageId;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "chat_room_user", joinColumns = @JoinColumn(name = "chat_room_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> joinedUsers = new ArrayList<>();
    
    @Column(name = "direct_chat_id", length=32)
    private String directRoomId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Long createdUserId) {
        this.createdUserId = createdUserId;
    }

    public List<User> getJoinedUsers() {
        return joinedUsers;
    }

    public void setJoinedUsers(List<User> joinedUsers) {
        this.joinedUsers = joinedUsers;
    }
    
    public void addUsers(List<User> users) {
        this.joinedUsers.addAll(users);
    }
    
    public void addUsers(User user) {
        this.joinedUsers.add(user);
    }

    public Date getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(Date lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public Long getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(Long lastMesssageId) {
        this.lastMessageId = lastMesssageId;
    }

    public String getDirectRoomId() {
        return directRoomId;
    }

    public void setDirectRoomId(String directRoomId) {
        this.directRoomId = directRoomId;
    }
}
