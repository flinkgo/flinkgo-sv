package com.flinkgo.socket.model.message.dto;

public abstract class ChatMessageAbstract {
    private String message;
    
    public ChatMessageAbstract() {
    
    }
    
    public ChatMessageAbstract(ChatMessageAbstract msg) {
        this.message = msg.getMessage();
    }
    
    public ChatMessageAbstract(String msg) {
        this.setMessage(msg);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
