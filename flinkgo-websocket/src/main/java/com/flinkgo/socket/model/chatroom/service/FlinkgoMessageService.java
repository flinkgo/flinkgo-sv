package com.flinkgo.socket.model.chatroom.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flinkgo.auth.repos.user.UserRepository;
import com.flinkgo.auth.utilities.RestCustomException;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.model.chatroom.ChatRoomMessage;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;
import com.flinkgo.socket.repository.chatroom.ChatRoomMessageRepository;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

@Service
public class FlinkgoMessageService implements ChatMessageService {
    Logger log = LoggerFactory.getLogger(FlinkgoMessageService.class);

    @Autowired
    ChatRoomRepository roomRepo;

    @Autowired
    ChatRoomMessageRepository msgRepo;

    @Autowired
    UserRepository userRepos;

    @Autowired
    ChatRoomService roomService;
    
    @Autowired
    ObjectMapper mapper;

    /**
     * @param req.message - chat message
     * @param req.userId - id of chat user
     * @param req.userName - name of chat user
     * @param req.directMessage - if message is direct
     * @param req.roomId - id of room if is chatroom message
     * @return entity message
     */
    @Override
    public ChatRoomMessage saveChatMessage(ChatMessageCreation req) {
        try {
            log.warn(mapper.writeValueAsString(req));
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // check user exist or not
        userRepos.findById(req.getFromUserId()).orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);

        ChatRoom chatRoom;

        ChatRoomMessage msg;
        if (req.isDirectMessage()) {
            msg = createDirectMessage(req.getMessage(), req.getFromUserId(), req.getFromUserName());

            chatRoom = roomService.createDirectChatRoom(req.getFromUserId(), req.getToUserId()).getRoom();
            
        } else {
            // check room exist or not
            chatRoom = roomRepo.findById(req.getRoomId()).orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);

            msg = createRoomMessage(req.getMessage(), req.getFromUserId(), req.getFromUserName(), req.getRoomId());
        }
        
        msg.setChatRoomId(chatRoom.getId());
        msgRepo.save(msg);

        updateChatRoomLastMessage(chatRoom, msg.getId(), msg.getCreatedTime());

        return msg;
    }

    public ChatRoomMessage createDirectMessage(String message, Long userId, String userName) {
        if(StringUtils.isEmpty(userName)) {
            throw RestCustomException.PARAMATER_MISSING;
        }
        
        userRepos.findById(userId).orElseThrow(() -> RestCustomException.RESOURCE_NOT_FOUND);
        
        ChatRoomMessage chatRoomMessage = new ChatRoomMessage();
        chatRoomMessage.setUserId(userId);
        chatRoomMessage.setMessage(message);
        chatRoomMessage.setUserName(userName);
        chatRoomMessage.setCreatedTime(new Date());
        
        return chatRoomMessage;
    }

    /**
     * save message of chatroom
     * 
     * @param message  - chat message
     * @param userId   - id of chat user
     * @param userName - name of chat user
     * @return entity message
     */
    public ChatRoomMessage createRoomMessage(String message, Long userId, String userName, Long roomId) {

        if (StringUtils.isEmpty(userName)) {
            throw RestCustomException.PARAMATER_MISSING;
        }

        ChatRoomMessage chatRoomMessage = new ChatRoomMessage();
        chatRoomMessage.setMessage(message);
        chatRoomMessage.setUserId(userId);
        chatRoomMessage.setUserName(userName);
        chatRoomMessage.setChatRoomId(roomId);
        chatRoomMessage.setCreatedTime(new Date());

        return chatRoomMessage;
    }

    @Transactional
    private ChatRoom updateChatRoomLastMessage(ChatRoom room, Long messageId, Date messageTime) {

        if (room == null) {
            throw RestCustomException.RESOURCE_NOT_FOUND;
        }

        room.setLastMessageId(messageId);
        room.setLastMessageTime(messageTime);
        
        log.info("update room: " + room.getId());

        return roomRepo.save(room);
    }
}
