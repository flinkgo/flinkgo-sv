package com.flinkgo.socket.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.flinkgo.auth.dto.ResponseSuccess;
import com.flinkgo.socket.model.chatroom.dto.ChatMessageCreation;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomCreateRequest;
import com.flinkgo.socket.model.chatroom.dto.ChatRoomView;
import com.flinkgo.socket.model.chatroom.service.ChatMessageService;
import com.flinkgo.socket.model.chatroom.service.ChatRoomService;

@RestController
@RequestMapping("/room")
public class ChatRoomController {
    static final Logger log = LoggerFactory.getLogger(ChatRoomController.class);

    @Autowired
    ChatRoomService roomService;

    @Autowired
    ChatMessageService chatMessageService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<Object> createRoom(@RequestBody @Valid ChatRoomCreateRequest req) {

        ChatRoomView view = roomService.createChatRoom(req);

        ResponseSuccess res = new ResponseSuccess(HttpStatus.ACCEPTED.value(), "success");
        res.addResults("room", view);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Object> getListRooms(@RequestParam Integer page, @RequestParam Integer size) {

        List<ChatRoomView> rooms = roomService.getChatRoomView(page, size);

        ResponseSuccess res = new ResponseSuccess(HttpStatus.OK.value(), "success");
        res.addResults("room", rooms);
        return new ResponseEntity<Object>(res, HttpStatus.valueOf(res.getStatus()));
    }

    @GetMapping("/add-user")
    public ResponseEntity<Object> addUserIntoRoom(@RequestParam long userId, @RequestParam long roomId) {
        roomService.addUserToChatRoom(userId, roomId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @PostMapping("/save-message-room")
    public ResponseEntity<Object> saveMessageRoom(@RequestBody @Valid ChatMessageCreation req) {
        chatMessageService.saveChatMessage(req);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

}
