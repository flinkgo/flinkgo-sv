package com.flinkgo.socket.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.flinkgo.socket.amqp.MessageProducer;

@RestController
@RequestMapping("/test")
public class TestController {
    
    @Autowired
    MessageProducer msgProducer;

    @GetMapping
    public String hello() {
        return "websocket API online";
    }
    
    @GetMapping("send")
    public String sendMessage(@RequestParam String msg) {
        
        msgProducer.sendTestMessage(msg);
        return msg;
    }
}
