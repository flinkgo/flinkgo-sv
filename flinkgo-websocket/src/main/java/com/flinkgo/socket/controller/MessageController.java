package com.flinkgo.socket.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.services.token.TokenService;
import com.flinkgo.socket.model.message.dto.ReturnedMessage;
import com.flinkgo.socket.model.message.dto.UploadMessage;
import com.flinkgo.socket.model.message.service.ChatMessageService;

@Controller
public class MessageController {
    public static final Logger log = LoggerFactory.getLogger(MessageController.class);
    
    @Autowired
    TokenService tokenService;
    
    @Autowired
    ChatMessageService msgService;
    
    @Autowired
    ObjectMapper mapper;
    
    @MessageMapping("/greetings")
    @SendTo("/topic/greetings")
    public ReturnedMessage greeting(String message, Principal principal) throws Exception {
        
        return new ReturnedMessage(message, mapper.readValue(principal.getName(), AuthUser.class));
    }
    
    @MessageMapping("/private")
    public void sendPrivateMessage(UploadMessage message, Principal principal) throws Exception {
        
        msgService.sendMessageToUser(message, mapper.readValue(principal.getName(), AuthUser.class));
    }
    
    @MessageMapping("/room/{roomId}")
    public void sendMessageToRoom(UploadMessage message, Principal principal,
            @DestinationVariable("roomId") Long roomId) throws Exception {
        
        msgService.sendMessageToRoom(message, mapper.readValue(principal.getName(), AuthUser.class), roomId);
    }
}
