package com.flinkgo.socket.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.flinkgo.auth.config.security.FlinkgoAuthenticationToken;

public class WebsocketTokenAuthenticationFilter extends GenericFilterBean {
    static final Logger log = LoggerFactory.getLogger(WebsocketTokenAuthenticationFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        
        String token = req.getParameter("auth");

        try {
            if (token == null) {
                throw new Exception();
            }
            
            log.info(token);
                               
            FlinkgoAuthenticationToken auth = 
                    new FlinkgoAuthenticationToken(null, token);

            
            SecurityContextHolder.getContext().setAuthentication(auth);

        } catch (Exception e) {
            
        } finally {
            chain.doFilter(request, response);
        }
        
    }

}
