package com.flinkgo.socket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;
import org.springframework.web.socket.handler.WebSocketHandlerDecoratorFactory;

import com.flinkgo.socket.config.websocket.FlinkgoLoggingWebsocketHandler;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    
    @Value("${spring.rabbitmq.host}")
    private String host;
    
    @Value("${spring.rabbitmq.virtual-host}")
    private String vhost;
    
    @Value("${spring.rabbitmq.username}")
    private String userName;
    
    @Value("${spring.rabbitmq.password}")
    private String password;
    
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableStompBrokerRelay("/topic", "/queue")
            .setAutoStartup(true)
            .setClientLogin(userName)
            .setClientPasscode(password)
            .setSystemLogin(userName)
            .setSystemPasscode(password)
            .setRelayHost(host)
            .setVirtualHost(vhost);
        
        config.setApplicationDestinationPrefixes("/chat");
        
        config.setUserDestinationPrefix("/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket").setAllowedOrigins("*");
    }
    
    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registry) {
        registry.addDecoratorFactory(factory());
    }
    
    @Bean
    WebSocketHandlerDecoratorFactory factory() {
        return new WebSocketHandlerDecoratorFactory() {
            @Override
            public WebSocketHandler decorate(WebSocketHandler handler) {
                return new FlinkgoLoggingWebsocketHandler(handler);
            }
        };
    }
}
