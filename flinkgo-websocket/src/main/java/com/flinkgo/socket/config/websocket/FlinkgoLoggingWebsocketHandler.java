package com.flinkgo.socket.config.websocket;

import java.util.logging.Logger;

import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.LoggingWebSocketHandlerDecorator;

public class FlinkgoLoggingWebsocketHandler extends LoggingWebSocketHandlerDecorator {
    static final Logger log = Logger.getLogger(FlinkgoLoggingWebsocketHandler.class.getSimpleName());
    
    public FlinkgoLoggingWebsocketHandler(WebSocketHandler delegate) {
        super(delegate);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        log.info("Payload:\n" + message.getPayload().toString());
        super.handleMessage(session, message);
    }
}
