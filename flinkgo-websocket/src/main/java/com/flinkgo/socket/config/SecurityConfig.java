package com.flinkgo.socket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.flinkgo.auth.config.security.FlinkgoAuthenticationEntryPoint;
import com.flinkgo.auth.config.security.FlinkgoTokenAuthenticationFilter;
import com.flinkgo.auth.config.security.FlinkgoUserDetailAuthenticationProvider;
import com.flinkgo.auth.services.token.TokenService;
import com.flinkgo.auth.services.user.UserService;
import com.flinkgo.socket.config.security.WebsocketTokenAuthenticationFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    UserService userService;
    
    @Autowired
    TokenService tokenService;
    
    @Bean
    public AuthenticationProvider authProvider() {
        return new FlinkgoUserDetailAuthenticationProvider(tokenService);
    }
    
    @Bean
    public AuthenticationEntryPoint entry() {
        return new FlinkgoAuthenticationEntryPoint();
    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider());
    }
    
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .httpBasic().disable()
            .cors().disable()
            .csrf().disable()
            .exceptionHandling().authenticationEntryPoint(entry())
            .and()
            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .and()
            .authorizeRequests()
                .antMatchers("/room/").authenticated()
                .antMatchers("/*", "/user/register", "/test/**").permitAll()                
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/user/*").hasAnyAuthority("user", "admin")
                .antMatchers("/websocket/**").hasAnyAuthority("user", "admin")
                .antMatchers("/test").permitAll()
                .anyRequest().authenticated()
            .and()
            .logout()
                .logoutUrl("/logout").invalidateHttpSession(true)
                .logoutSuccessUrl("/")
            .and()
            .addFilterBefore(new WebsocketTokenAuthenticationFilter(), 
                    UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(new FlinkgoTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
            ;
        super.configure(http);
    }
}
