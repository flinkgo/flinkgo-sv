package com.flinkgo.socket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

import com.flinkgo.socket.config.websocket.ChatroomSubscriptionInterceptor;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

@Configuration
public class SocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {
    
    @Autowired
    ChatRoomRepository roomRepo;
    
    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
            .simpTypeMatchers(SimpMessageType.CONNECT, 
                    SimpMessageType.UNSUBSCRIBE, SimpMessageType.DISCONNECT).authenticated()
            .simpDestMatchers("/queue/**", "/room/").denyAll()
            .simpSubscribeDestMatchers("/topic/**","/chat/**").authenticated()
            .anyMessage().permitAll();
    }
    
    @Override
    protected void customizeClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(subscriptionInterceptor());
        
        super.customizeClientInboundChannel(registration);
    }
    
    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }
    
    @Bean
    ChatroomSubscriptionInterceptor subscriptionInterceptor() {
        return new ChatroomSubscriptionInterceptor(roomRepo);
    }
}