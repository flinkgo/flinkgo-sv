package com.flinkgo.socket.config.amqp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {
    private static final Logger log = LoggerFactory.getLogger(RabbitConfiguration.class);
    
    @Value("${spring.rabbitmq.host}")
    private String hostname;
    
    @Value("${spring.rabbitmq.username}")
    private String username;
    
    @Value("${spring.rabbitmq.password}")
    private String password;
    
    @Value("${flinkgo.rabbitmq.virtual-host}")
    private String vhost;
    
    public final static String TOPIC = "flinkgo_topic";
    public final static String SAVE_MSG_QUEUE = "flinkgo_chat_msg";
    public final static String TEST_QUEUE = "flinkgo_test";
    
    @Bean
    Queue saveMessageQueue() {
        return new Queue(SAVE_MSG_QUEUE, true);
    }
    
    @Bean
    Queue testQueue() {
        return new Queue(TEST_QUEUE, false);
    }
    
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(TOPIC);
    }
    
    @Bean
    Binding binding() {
        return BindingBuilder.bind(saveMessageQueue()).to(exchange()).with(SAVE_MSG_QUEUE);
    }
    
    @Bean
    Binding testBinding() {
        return BindingBuilder.bind(testQueue()).to(exchange()).with(TEST_QUEUE);
    }
    
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory conn = new CachingConnectionFactory(hostname);
        
        log.warn(username);
        conn.setUsername(username);
        conn.setPassword(password);
        conn.setVirtualHost(vhost);
        
        return conn;
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public AmqpTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setMessageConverter(producerJackson2MessageConverter());
        
        return template;
    }
    
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
