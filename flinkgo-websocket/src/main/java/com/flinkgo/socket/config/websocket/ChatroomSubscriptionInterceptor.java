package com.flinkgo.socket.config.websocket;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flinkgo.auth.dto.user.AuthUser;
import com.flinkgo.auth.model.user.User;
import com.flinkgo.socket.model.chatroom.ChatRoom;
import com.flinkgo.socket.repository.chatroom.ChatRoomRepository;

@Component("FlinkgoSubscriptionInterceptor")
public class ChatroomSubscriptionInterceptor implements ChannelInterceptor {
    static final Logger log = LoggerFactory.getLogger("SubcribtionInterceptor");
    
    ObjectMapper mapper = new ObjectMapper();
    
    ChatRoomRepository roomRepo;
    
    public ChatroomSubscriptionInterceptor(ChatRoomRepository roomRepo) {
        this.roomRepo = roomRepo;
    }
    
    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(message);
        if(StompCommand.SUBSCRIBE.equals(accessor.getCommand())) {
            AuthUser user;
            try {
                user = mapper.readValue(accessor.getUser().getName(), AuthUser.class);
            } catch (IOException | NullPointerException e) {
                throw new IllegalArgumentException("Access denied for this topic");
            }
            
            if (accessor.getDestination().matches("/topic/room\\..+")) {
                Integer roomId = Integer.parseInt(accessor.getDestination().substring(12));
                
                Optional<ChatRoom> room = roomRepo.findById(roomId.longValue());
                List<Long> userIds = room.map(chatRoom -> {
                    return chatRoom.getJoinedUsers();
                }).orElseThrow(() -> new IllegalArgumentException("Access denided for this topic"))
                        .stream().map(User::getId).collect(Collectors.toList());
                
                if (!userIds.contains(user.getUserId())) {
                    throw new IllegalArgumentException("Access denied for this topic");
                }
            }
        }
        
        return ChannelInterceptor.super.preSend(message, channel);
    }
}
