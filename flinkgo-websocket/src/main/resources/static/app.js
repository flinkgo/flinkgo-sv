var stompClient = null;
const urlParams = new URLSearchParams(window.location.search);
const url = window.location;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#token").prop("disabled", connected);
    
    $("#disconnect").prop("disabled", !connected);
    $("#token").prop("disabled", connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    //var socket = new SockJS("/websocket");
    //stompClient = Stomp.over(socket);
	var token = $("#token").val();
	stompClient = Stomp.client("ws://" + url.hostname + ":" + url.port +"/websocket?auth=" + token);
	stompClient.heartbeat.outgoing = 0;
	stompClient.heartbeat.incoming = 0;
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/reply', function (greeting) {
            showGreeting(greeting.body);
        })
    }, function (error) {
        console.log(error);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/chat/private", {}, $("#name").val());
    $("#name").val("");
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
});

